package br.com.mauda.seminario.cientificos.exception;

public class ObjetoNuloException extends SeminariosCientificosException {

    private static final long serialVersionUID = 4928599035264976611L;

    public ObjetoNuloException() {
        super("ER0003");
    }

    public ObjetoNuloException(Throwable t) {
        super(t);
    }

    // throw new SeminariosCientificosException ("ER0003");
    // throw new ObjetoNuloException();
}
